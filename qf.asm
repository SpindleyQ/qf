   cs push \ copy code segment to data segment
   ds pop

\ print number
\ in: ax = number
\ clobbers: ax, cx, dx
   ax 123 # mov
   cx 10 # mov
:> moredigits
   dx dx xor
   cx div
   dl 48 # add
   ax push
   ax &h0200 # mov
   &h21 int
   ax pop
   moredigits jne

   0 .EXIT

   cx 11 # mov

\ Register allocation:
\ ax - top of stack value
\ ds:di - pointer above second value in stack
\ ss:sp - return stack pointer
\ ds:bx - instruction pointer
\ dx - current instruction (w) - clobberable

\ interpreter: indirect threaded
\ dictionary entry layout:
\ next (w) | charcount (b) | name (b+) | codeptr (w) | args...

:> @next
   dx [bx] mov
   bx 2 # add
   [dx] jmp
: .NEXT @next jmp ;

: lbl>list mkdoer currlbl @ , 0 currlbl ! 
  does> @ dup execute dlbl execute <: ;

:> @latest @latest lbl>list latest!
: outname! 
  tell 0 outb! 
  0 begin in@ dup [ in@  lit ] != while outb! 1 + repeat drop
  tell >rot swap seek outb! seek ;
: @mkword latest! outname! ;
: :asm @mkword tell 2 + tell>addr outw! ;

:asm ; bx pop .NEXT
:> $docolon
  bx push
  dx bx mov
  .NEXT


