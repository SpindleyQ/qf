:> end
   cs push \ copy code segment to data segment
   ds pop

\ read number
\ clobbers: ax, bx, cx, dx
   dx dx xor
   cx cx xor
   bx 10 # mov
   dx push
:> readnext
   ax &h0100 # mov
   &h21 int

   :> readover  
   al in@ 0 # cmp
   readover jl
   al in@ 9 # cmp
   readover jg
   cl al mov
   cl in@ 0 # sub
   ax pop
   bx mul
   ax cx add
   ax push
   readnext jmps
readover <:
   ax pop

   \ ax 0 # mov
\ print number
\ in: ax = number
\ clobbers: ax, bx, cx, dx
   cx 10 # mov
   bx bx xor

:> moredigits
   bx inc 
   dx dx xor
   cx div
   dx push
   ax 0 # cmp
   moredigits jne

:> moredigits
   dx pop
   dl in@ 0 # add
   ax &h0200 # mov
   &h21 int
   bx dec
   moredigits jne

   0 .EXIT

end <:
